<?php
/**
 * Déclaration des différentes variables utilisées
 */
$fichier = file($argv[1]);
$total = count($fichier);
$htmlNom = $argv[2];
$tableau = array();
$date = date("d-m-Y à H:i");

/**
 * Vérifie si le fichier contenant les IP existe et affiche un message de confirmation
 */
if (file_exists($argv[1])) {
    echo "\e[1;32mLe fichier $argv[1] contenant les IP existe.\e[0;m" . "\n";
} else {
    echo "\e[0;31m/!\ Le fichier $argv[1] contenant les IP n'existe pas.\e[0;m" . "\n";
}

/**
 * Vérifie si le fichier recevant le tableau html est défini
 */
if (empty($htmlNom))  {
    echo "\e[0;31m/!\ Il manque un fichier où écrire le résultat.\e[0;m" . "\n";
  } else {
      echo "\e[1;32mLe fichier $argv[2] va recevoir le résultat.\e[0;m" . "\n";
  }

/**
 * Création de deux variable possèdant chacun une partie du code html: html1->La partie haute du code
 *                                                                     html2->La partie haute du code
 */
$html = "<!DOCTYPE html>
            <html>
            <head><center>Test du reseau ".$date."</center></head>
            <style> 
                table{
                    margin-left:auto;
                    margin-right:auto;
                    background-color:#7a8380;
                    border-radius: 8px;
                }
                
            </style>
            <body style = 'background-color: #80cbb6';>
            <table>
                <tr><td>IP</td><td>Status</td></tr>";
                

$html2 = "  
            <tr><td></td><td></td></tr>
            </table>
            </body>
            </html>";

/**
 * Boucle "for" executant la fonction ping sur les différente IP entrer préalablement dans un fichier texte
 */
    for($i = 0; $i < $total; $i++)
    {
        $pingresult = exec("/bin/ping -c 3 ".$fichier[$i], $outcome, $status);
        /**
         * Vérification si les adresses IP entrée sont correctes où non
         */
        if (filter_var(gethostbyname(trim($fichier[$i])), FILTER_VALIDATE_IP)){ 


        /**
         * Affiche les messages adapter au différents résultat obtenu via la fonction "ping" 
         */
            if (0 == $status)
            {
                echo "l'address IP: $fichier[$i]" . "\e[1;32m[  OK  ]\e[0;m" . "\n";
                
                $tableau[$i] ="<tr><td style='background-color:#9fb0ac'>". $fichier[$i] . "</td><td style='background-color:#3bc03d'>" . "fonctionne </td></tr>";
                
                
            } else 
            {
                echo "l'address IP: $fichier[$i]" . "\e[0;31m[  ERREUR  ]\e[0;m" . "\n";
                $tableau[$i] ="<tr><td style='background-color:#9fb0ac'>" . $fichier[$i] . "</td><td style='background-color:#ba4331'>" . "ne fonctionne pas </td></tr>"  ;
            }
        }else{
            echo "l'address IP: $fichier[$i]" . "\e[0;33m[  PROBLEME  ]\e[0;m" . "\n";
                $tableau[$i] ="<tr><td style='background-color:#9fb0ac'>" . $fichier[$i] . "</td><td style='background-color:#d88e31'>" . "IP inexistante </td></tr>"  ;
        }
    }
    /**
     * Ouverture d'un fichier html et écriture des script html dans celui-ci
     * (Pour chaque IP tester une ligne est ajouter au tableau html )
     */
    $fp=fopen($htmlNom,"w");
    $texte = $html;
    foreach ($tableau as $value) {
        $texte = $texte . $value;
    }
        $texte = $texte . $html2;
        fwrite($fp, $texte);
        fclose($fp);

    /**
     * Commande pour envoyer le tableau html a une adresse mail spécifier
     */
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    mail("sio.bts@outlook.fr", "PROJET_PING", $texte, $headers);

?>

